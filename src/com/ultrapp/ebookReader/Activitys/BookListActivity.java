package com.ultrapp.ebookReader.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.ultrapp.ebookReader.Adapters.ListAdapter;
import com.ultrapp.ebookReader.R;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-19
 * Time: 下午5:20
 * To change this template use File | Settings | File Templates.
 */
public class BookListActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private String packageName;
    private ListView bookList;
    private String[] localFileList;
    private BaseAdapter mAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_list);
        packageName = getPackageName();
        try {
            localFileList = getAssets().list("documents");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        bookList = (ListView) findViewById(R.id.book_list);
        mAdapter =  new ListAdapter(this, localFileList);
        bookList.setAdapter(mAdapter);
        bookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("filename", localFileList[i]);
                intent.setClass(BookListActivity.this, ReadingActivity.class).putExtras(bundle);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume(){
        bookList.setAdapter(mAdapter);
        super.onResume();
    }

}
