package com.ultrapp.ebookReader.Activitys;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import com.ultrapp.ebookReader.Adapters.BookMarksListAdapter;
import com.ultrapp.ebookReader.Adapters.ListAdapter;
import com.ultrapp.ebookReader.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-29
 * Time: 下午4:21
 * To change this template use File | Settings | File Templates.
 */
public class BookMarkListActivity extends Activity {

    private String packageName;
    private ListView bookList;
    private String[] localFileList;
    private BaseAdapter mAdapter;
    private String fileName;
    private ArrayList<String> bookMarkNames;
    private ArrayList<Integer> markedPages;
    private ArrayList<Boolean> ifSelected;
    private Button manage_btn;
    private Button selectAll;
    private Button deleteSelected;
    private Button cancelEdit;
    private TextView noBookMark;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookmark_list_activity);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        fileName = bundle.getString("filename");
        initViews();
        initBookMarks();
        System.out.print("查询bookMarkTable：" + bookMarkNames.size());
        mAdapter =  new BookMarksListAdapter(this,bookMarkNames,markedPages, ifSelected, false);
        bookList.setAdapter(mAdapter);
        bookList.setOnItemClickListener(new onItemClickListenerNormal());

    }

    public void initViews(){
        bookList = (ListView) findViewById(R.id.book_list);
        manage_btn = (Button) findViewById(R.id.edit);
        selectAll = (Button) findViewById(R.id.select_all);
        deleteSelected = (Button) findViewById(R.id.delete);
        cancelEdit = (Button) findViewById(R.id.cancel);
        noBookMark = (TextView) findViewById(R.id.no_bookmark);
    }

    public void editBookmarks(View v){
        manage_btn.setVisibility(View.GONE);
        selectAll.setVisibility(View.VISIBLE);
        deleteSelected.setVisibility(View.VISIBLE);
        cancelEdit.setVisibility(View.VISIBLE);
        mAdapter =  new BookMarksListAdapter(this,bookMarkNames,markedPages, ifSelected, true);
        bookList.setAdapter(mAdapter);
        bookList.setOnItemClickListener(new onItemClickListenerSelect());

    }

    public void setSelectAll(View v){
        for(int i = 0; i < ifSelected.size(); i++){
            ifSelected.set(i, true);
        }
        mAdapter.notifyDataSetChanged();
    }

    public void deleteSelected(View v){
        for(int i = 0; i < ifSelected.size(); i++){
            if(ifSelected.get(i)){
                ReadingActivity.bookMarkTable.delete("bookmarkname = ?", new String[]{bookMarkNames.get(i)});
            }
        }
        Cursor results = ReadingActivity.bookMarkTable.getLatestRows(null);
        if(results.moveToFirst()){
            for(int i = 0; i < results.getCount(); i++){
                bookMarkNames.add(results.getString(1));
                markedPages.add(results.getInt(2));

                results.moveToNext();
            }
        }
        mAdapter.notifyDataSetChanged();

    }

    public void cancleEdit(View v){
        manage_btn.setVisibility(View.VISIBLE);
        selectAll.setVisibility(View.GONE);
        deleteSelected.setVisibility(View.GONE);
        cancelEdit.setVisibility(View.GONE);
        mAdapter =  new BookMarksListAdapter(this,bookMarkNames,markedPages, ifSelected, false);
        bookList.setAdapter(mAdapter);
        bookList.setOnItemClickListener(new onItemClickListenerNormal());
    }

    class onItemClickListenerNormal implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //To change body of implemented methods use File | Settings | File Templates.
            ContentValues cv = new ContentValues();
            cv.put("lastposition", markedPages.get(i));
            LoadActivity.novalTable.updateValues(cv, "filename = ?", new String[]{fileName});
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("filename", fileName);
            intent.setClass(BookMarkListActivity.this, ReadingActivity.class).putExtras(bundle);
            startActivity(intent);
            BookMarkListActivity.this.finish();
        }
    }

    class onItemClickListenerSelect implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    private void initBookMarks(){
        bookMarkNames = new ArrayList<String>();
        markedPages = new ArrayList<Integer>();
        ifSelected = new ArrayList<Boolean>();
        Cursor results = ReadingActivity.bookMarkTable.getLatestRows(null);

            if(results.moveToFirst()){
                for(int i = 0; i < results.getCount(); i++){
                    bookMarkNames.add(results.getString(1));
                    markedPages.add(results.getInt(2));
                    System.out.print("书签名：" + results.getString(1) + "位置：" + results.getInt(2));
                    results.moveToNext();
                }
        }
        System.out.print("查询bookMarkTable：" + bookMarkNames.size());
        if(bookMarkNames.size() == 0){
            noBookMark.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK) {
                    // 监控返回键
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString("filename", fileName);
                    intent.setClass(BookMarkListActivity.this, ReadingActivity.class).putExtras(bundle);
                    startActivity(intent);
                    BookMarkListActivity.this.finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
