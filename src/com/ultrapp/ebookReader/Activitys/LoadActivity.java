package com.ultrapp.ebookReader.Activitys;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import com.ultrapp.ebookReader.Database.DataBaseHelper;
import com.ultrapp.ebookReader.Database.Table.Table;
import com.ultrapp.ebookReader.R;

import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-19
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */
public class LoadActivity extends Activity {
    private String packageName;
    public static Table novalTable;
    public static SQLiteDatabase mSQLiteDatabase;
    private String[] fileNames;
    String[] novelNames;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_activity);
        packageName = getPackageName();
        Resources res =getResources();
        fileNames =res.getStringArray(R.array.filenames);
        novelNames =res.getStringArray(R.array.novelnames);
        initDatabaseTables();
        mAsyncTask bTask = new mAsyncTask();
        bTask.execute();



    }

    private void initDatabaseTables(){
        mSQLiteDatabase = (new DataBaseHelper(this)).getWritableDatabase();

        novalTable = new Table("novaltable", mSQLiteDatabase);
        novalTable.addColumn("filename", "char", "primary key");
        novalTable.addColumn("novelname", "char", "");
        novalTable.addColumn("lastposition", "long", "");
        novalTable.addColumn("pagesplite", "string", "");
        novalTable.create();
    }

    private void initDatabaseData(String fileName){

        ContentValues cv = new ContentValues();
        cv.put("filename", fileName);
        for(int i = 0; i < fileNames.length; i++){
            if(fileNames[i].equals(fileName))
                cv.put("novelname", novelNames[i]);
        }
        cv.put("lastposition", "");
        cv.put("pagesplite", "");
        novalTable.insert(cv);
    }


    private String FILE_EXCUTED = "excuted";
    private String DID_NOTHING = "nothing";

    private String initBooks() throws IOException {
        String result = DID_NOTHING;
        String[] assetsFileList ;
//        File[] localFileList;

        assetsFileList = getAssets().list("documents");
        Cursor savedFileName = novalTable.queryColumn(new String[]{"filename"});
        String[] localFileNames = new String[savedFileName.getCount()];
        if(savedFileName.moveToFirst()){
            for(int i = 0; i < savedFileName.getColumnCount(); i++){
                localFileNames[i] = savedFileName.getString(0);
                savedFileName.moveToNext();
            }
        }
        System.out.println(localFileNames.length);
        savedFileName.close();
//        localFileList = new File("/data/data/" + packageName + "/files/").listFiles();
        if(localFileNames.length == 0){
            for(String name : assetsFileList){
                System.out.println("file:" + name);
                InputStream input = getAssets().open("documents/" + name);
                byte[] mByte = new byte[input.available()];
                input.read(mByte);
                FileOutputStream outStream=this.openFileOutput(name, Context.MODE_WORLD_READABLE);
                outStream.write(mByte);
                outStream.close();
                input.close();
                initDatabaseData(name);
            }
            result = FILE_EXCUTED;
        }else{
            ArrayList<String> localFileNameList = new ArrayList<String>();
            for(String temp: localFileNames){
                localFileNameList.add(temp);
            }
            for(String name : assetsFileList){
                if(!localFileNameList.contains(name)){
                    System.out.println("file:" + name);
                    InputStream input = getAssets().open("documents/" + name);
                    byte[] mByte = new byte[input.available()];
                    input.read(mByte);
                    FileOutputStream outStream=this.openFileOutput(name, Context.MODE_WORLD_READABLE);
                    outStream.write(mByte);
                    outStream.close();
                    input.close();
                    initDatabaseData(name);
                    result = FILE_EXCUTED;
                }

            }
            ArrayList<String> assetsFileNameList = new ArrayList<String>();
            for(String temp: assetsFileList){
                assetsFileNameList.add(temp);
            }

            for(String name : localFileNameList){
                if(!assetsFileNameList.contains(name)){
                    System.out.println("file:" + name);
                    novalTable.delete("filename = ?", new String[]{name});
                }

            }
        }
        return result;
    }

    class mThread extends Thread{
        public void run() {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    class mAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            String result = DID_NOTHING;
            try {
                result = initBooks();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return result;

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String result) {
            // 返回HTML页面的内容
            mThread time = new mThread();
            time.start();

            Intent intent = new Intent();
            intent.setClass(LoadActivity.this, BookListActivity.class);
            startActivity(intent);
            LoadActivity.this.finish();


        }

        @Override
        protected void onPreExecute() {
            // 任务启动，可以在这里显示一个对话框，这里简单处理

        }

//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            // 更新进度
//
//        }
    }
}
