package com.ultrapp.ebookReader.Activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import com.ultrapp.ebookReader.Adapters.ArticleViewPagerAdapter;
import com.ultrapp.ebookReader.Database.Table.Table;
import com.ultrapp.ebookReader.MyView;
import com.ultrapp.ebookReader.R;
import com.ultrapp.ebookReader.Utils.DensityUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-21
 * Time: 下午3:52
 * To change this template use File | Settings | File Templates.
 */
public class ReadingActivity extends Activity {
    private TextView batteryLife;
    private TextView time;
    private TextView readedRate;
//    private TextView content;
    private boolean isShowing ;
    private Handler handler = new Handler();
    private String DEFAULT_TIME_FORMAT = "hh:mm";
    private String currentTime;

    private int len = 0;
    private InputStream mInputStream;
    private int m_fontSize = 20;
    public HashMap<Integer, int[]> pageDetail;
    private String packageName;
    public Paint mPaint;
    public int ScreenHeight;
    public int ScreenWidth;
    public int ViewHeight;
    public int ViewWidth;
    private int adsHeight = 50;

    public MappedByteBuffer m_mbBuf;
    private long fileLength;
    private long readed = 0;
    public int textSize = 20;
    private int textSizeInPx;
    private int topTextSize = 10;
    public int linesCount = 0;
//    private PageTask task;
    private int pageCount = 1;
    public String charSet = "UTF-8";
    private LinearLayout linearLayout;
    private Canvas mCanvas;
    public Bitmap mBitMap;
//    private MyView myView;
    private ViewPager mViewPager;
    private int sidePadding = 10;
    private int sidePaddingInPx;
    private String fileName;

    public JSONArray indexInJSON;

    private AlertDialog processDia;
    private TextView processRate;
    private int currentPage;
    private AlertDialog menuDialog;
    public String bookMarkTitle;

    public static Table bookMarkTable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFile();
        initBookMarkDatabase();
        initBookMarks();
        setContentView(R.layout.reading_activity);
        calcViewSize();
        initViews();
        initDialogs();
        try {
            getBookIndex();
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    private void calcViewSize(){
        int topTextSizeInPx = DensityUtil.dip2px(ReadingActivity.this, textSize);
        int topTextPaddingInPx = DensityUtil.dip2px(ReadingActivity.this, 20);
        int adsHeightInPx = DensityUtil.dip2px(ReadingActivity.this, adsHeight);
        sidePaddingInPx = DensityUtil.dip2px(ReadingActivity.this,sidePadding);
        textSizeInPx = DensityUtil.dip2px(ReadingActivity.this, textSize);
        Display display = getWindowManager().getDefaultDisplay();
        ScreenHeight = display.getHeight();
        ScreenWidth = display.getWidth();
        ViewHeight = ScreenHeight - topTextSizeInPx - adsHeightInPx - topTextPaddingInPx;
        ViewWidth = ScreenWidth - sidePaddingInPx*2;
        linesCount = ViewHeight/textSizeInPx;
//        System.out.println("区域宽：" + ViewWidth);
//        System.out.println("理论显示字数：" + ViewWidth/textSizeInPx);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setTextSize(textSizeInPx);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);

//        mBitMap = Bitmap.createBitmap(ScreenWidth, ViewHeight, Bitmap.Config.ARGB_8888);
//        mCanvas = new Canvas(mBitMap);



    }

    private void getFile(){
        packageName = getPackageName();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        fileName = bundle.getString("filename");
        File book_file = new File("/data/data/" + packageName + "/files/" + fileName);

        isShowing = true;
        try {
            long lLen = book_file.length();
            fileLength = lLen;
            m_mbBuf = new RandomAccessFile(book_file, "r").getChannel().map(
                    FileChannel.MapMode.READ_ONLY, 0, lLen);

            System.out.println("fileLength:" +  lLen);

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void initBookMarkDatabase(){
        String bookName = fileName.substring(0, fileName.indexOf("."));
        bookMarkTable = new Table(bookName + "BookMarks", LoadActivity.mSQLiteDatabase);
        bookMarkTable.addColumn("id", "integer", "primary key AUTOINCREMENT");
        bookMarkTable.addColumn("bookmarkname", "char", "");
        bookMarkTable.addColumn("markedpage", "int", "");
        bookMarkTable.create();
    }

    private ArrayList<String> bookMarkNames;
    private ArrayList<Integer> markedPages;

    private void initBookMarks(){
        bookMarkNames = new ArrayList<String>();
        markedPages = new ArrayList<Integer>();
        Cursor results = bookMarkTable.query("id = ?", new String[] {"*"});
        if(results.moveToFirst()){
            for(int i = 0; i < results.getCount(); i++){
                bookMarkNames.add(results.getString(1));
                markedPages.add(results.getInt(2));
                results.moveToNext();
            }
        }
    }

    private void getBookIndex() throws JSONException {
        Cursor result = LoadActivity.novalTable.query("filename=?", new String[] {fileName});
        if(result.moveToFirst()){
            if(result.getString(3).equals("")){
                indexInJSON = new JSONArray();
                new PageTask().execute();
            }else {
                if(result.getString(2).equals("")){
                    currentPage = 1;
                }else{
                    currentPage = Integer.parseInt(result.getString(2));
                }

                indexInJSON = new JSONArray(result.getString(3));
                readedRate.setText(currentPage + "/"+ (indexInJSON.length() - 1));
                mViewPager.setAdapter(new ArticleViewPagerAdapter(getFragmentManager(),indexInJSON.length() - 1));
                mViewPager.setOnPageChangeListener(new MyOnPageChangedListener() );
                mViewPager.setCurrentItem(currentPage - 1);

            }

        }
    }

    private void initViews(){
//        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        batteryLife = (TextView) findViewById(R.id.battery_life);
        time = (TextView) findViewById(R.id.time);
        readedRate = (TextView) findViewById(R.id.rate);
        mViewPager = (ViewPager) findViewById(R.id.content);
//        content = (TextView) findViewById(R.id.content);

//        linearLayout.setPadding(textSizeInPx,0,textSizeInPx,0);

//        batteryLife.setTextColor(android.R.color.white);
        batteryLife.setTextSize(topTextSize);

//        time.setTextColor(android.R.color.white);
        time.setTextSize(topTextSize);

//        readedRate.setTextColor(android.R.color.white);
        readedRate.setTextSize(topTextSize);




//        content.setTextSize(textSize);

//        myView = new MyView(this);
//        myView.setMbitmap(mBitMap);
//        relativeLayout.addView(myView);

        handler.post(updateThread);
        registerReceiver(batteryChangedReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    private Button addBookMark;
    private Button bookMarkList;

    private void initDialogs(){
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.process_dialog,null);
        processRate = (TextView) dialogLayout.findViewById(R.id.process_rate);
        processDia = new AlertDialog.Builder(this).setTitle("载入中").setView(dialogLayout).create();

        View menuDialogLayout = inflater.inflate(R.layout.menu_dialog,null);
        Button readRate = (Button) menuDialogLayout.findViewById(R.id.readRateBtn);
        addBookMark = (Button) menuDialogLayout.findViewById(R.id.addBookMarkbtn);
        bookMarkList = (Button) menuDialogLayout.findViewById(R.id.bookMarkListBtn);
        Button readMode = (Button) menuDialogLayout.findViewById(R.id.readModeBtn);
        addBookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                ContentValues cv = new ContentValues();
                cv.put("bookmarkname", bookMarkTitle);
                cv.put("markedpage", currentPage);
                bookMarkTable.insert(cv);
                menuDialog.dismiss();

            }
        });
        if(bookMarkNames.size() == 0){
            bookMarkList.setEnabled(false);
        }
        bookMarkList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("filename", fileName);
                Cursor results = ReadingActivity.bookMarkTable.getLatestRows(null);

                if(results.moveToFirst()){
                    for(int i = 0; i < results.getCount(); i++){
                        System.out.print("书签名：" + results.getString(1) + "位置：" + results.getInt(2));
                        results.moveToNext();
                    }
                }
                intent.setClass(ReadingActivity.this, BookMarkListActivity.class).putExtras(bundle);
                startActivity(intent);
                menuDialog.dismiss();
                ReadingActivity.this.finish();
            }
        });
        menuDialog = new AlertDialog.Builder(this).setView(menuDialogLayout).create();
    }


    Runnable updateThread = new Runnable(){
        //将要执行的操作写在线程对象的run方法当中
        public void run(){
            handler.postDelayed(updateThread, 1000);
            //调用Handler的postDelayed()方法
            //这个方法的作用是：将要执行的线程对象放入到队列当中，待时间结束后，运行制定的线程对象
            //第一个参数是Runnable类型：将要执行的线程对象
            //第二个参数是long类型：延迟的时间，以毫秒为单位
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
            currentTime = dateFormatter.format(Calendar.getInstance().getTime());
            time.setText(currentTime);
        }
    };

    public void openMenu(View v){

        menuDialog.show();
    }

    private BroadcastReceiver batteryChangedReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            if(Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 100);
                batteryLife.setText("电量:" + (level * 100 / scale) + "%");
            }
        }
    };

    public void setUpBook() throws IOException, JSONException {
        JSONObject pageIndex = new JSONObject();
        long pageStart = 0;
        long pageLength = 0;
        int readedLinesCount = 0;
        pageStart = readed ;
        while(readedLinesCount < linesCount ){

            String paragraph = getParagraph();
            pageLength += paragraph.getBytes(charSet).length;
//            if(paragraph.contains("\n")){
//                System.out.println("换行位置：" + paragraph.indexOf("\n"));
//                readedLinesCount ++;
//            }

            // 如果是空白行，直接添加
            if (paragraph.length() == 0) {
                readedLinesCount ++;
            }
            while (paragraph.length() > 0) {
                // 画一行文字
                int nSize = mPaint.breakText(paragraph, true, ViewWidth,
                        null);

                if(paragraph.length() > nSize){
                    String ss = "！，。？；：’”）》】";
                    while(ss.contains(paragraph.substring(nSize,nSize+1))){
                        nSize = nSize - 1 ;
                    }
                }

//                if(pageCount < 5){
//                    System.out.println("计算每行字符：" +nSize);
//                    System.out.println("LinesCount:"+ readedLinesCount + "/" + linesCount
//                            + "\n Content:" + paragraph.substring(0,nSize));
//                }

                readedLinesCount++;

                    paragraph = paragraph.substring(nSize);

                if (readedLinesCount >= linesCount) {
                    break;
                }
            }
            if (paragraph.length() != 0) {
                pageLength -= paragraph.getBytes(charSet).length;

            }


        }

//            System.out.println("Start:" + pageStart);
//            System.out.println("End:" + (pageStart + pageLength));
//            System.out.println("Count:" + pageCount);
        pageIndex.put("pageStart", pageStart);
        pageIndex.put("pageLength", pageLength);
        readed = pageStart + pageLength;
        indexInJSON.put(pageCount,pageIndex);
        pageCount++;


//        return page;
    }



    public String getParagraph() throws IOException {

        int len = 0;    //所有读取的内容都使用temp接收
        long start = readed ;

        try{
            while(readed < fileLength){    //当没有读取完时，继续读取
//            System.out.println("read:" + m_mbBuf.get(readed));
                len++;
                readed++;
                if(m_mbBuf.get((int)(readed)) == 10){
//                len++;
//                readed++;
                    break;
                }


            }
        }catch(IndexOutOfBoundsException e){

        }

        byte b[] = new byte[len];
        for(int i = 0; i < len; i++){
            b[i] = m_mbBuf.get(i + (int)start);
//            System.out.println("byte:" + b[i]);
        }
//        System.out.println("start:"+start);
//        System.out.println("readed:"+readed);
//        System.out.println("len:"+len);
        return new String(b,charSet);
    }

    class MyOnPageChangedListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int i, float v, int i2) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void onPageSelected(int i) {
            //To change body of implemented methods use File | Settings | File Templates.
            readedRate.setText((i+1)+"/"+ (indexInJSON.length() - 1));
            currentPage = i+1;
            if(markedPages.contains(currentPage)){
                addBookMark.setEnabled(false);
            }else {
                addBookMark.setEnabled(true);
            }

        }

        @Override
        public void onPageScrollStateChanged(int i) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    class PageTask extends AsyncTask<String, Integer, String> {
        // 可变长的输入参数，与AsyncTask.exucute()对应

        @Override
        protected String doInBackground(String... params) {

            try {
                float s = 0;
                float a = 0;
                float b = fileLength;
                while(readed < fileLength){
                    setUpBook();

                    a = readed;
                    s = a/b;
                    publishProgress((int)(s*100));
                }

//                for(int i = 0; i < index.size(); i++){
//                    int pageNum = i + 1;
//                    System.out.println("Page:" + pageNum + " Start:" + index.get(pageNum)[0] + " End:" + index.get(pageNum)[1]);
//
//                }
            } catch (IOException e) {
                System.out.println("Erro:"+ e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            return null;

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
//            mCanvas.drawText("请稍等，正在处理" , 30, textSize * 5, mPaint);
        }

        @Override
        protected void onPostExecute(String result) {
            // 返回HTML页面的内容
            System.out.println("enddddddd");
            readedRate.setText("1/" + (indexInJSON.length() - 1));
            currentPage = 1;
            mViewPager.setAdapter(new ArticleViewPagerAdapter(getFragmentManager(),indexInJSON.length() - 1));
            mViewPager.setOnPageChangeListener(new MyOnPageChangedListener() );
            ContentValues cv = new ContentValues();
            cv.put("pagesplite", indexInJSON.toString());

            LoadActivity.novalTable.updateValues(cv , "filename = ?", new String[] {fileName});
            processDia.dismiss();
//            System.out.println(indexInJSON);
//            relativeLayout.setOnTouchListener(new mTouchListener());
//            try {
////                String ss = getPage(1);
////                content.setText(ss);
//                    drawPage(1);
////                System.out.println(ss);
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            } catch (IOException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
        }

        @Override
        protected void onPreExecute() {
            // 任务启动，可以在这里显示一个对话框，这里简单处理
            processDia.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // 更新进度
            processRate.setText(values[0]+ "%");
//            System.out.println(values[0] + "%");
//            content.setText("正在处理" +  values[0]+ "%");

//            mCanvas.save();
//            mCanvas.drawText("正在处理" +  values[0]+ "%", 0, textSize,mPaint);
//            mCanvas.restore();

        }

    }



    private float startPointX ;
    private float startPointY ;

    private float endPointX ;
    private float endPointY ;

    class mTouchListener implements View.OnTouchListener{

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            float distance ;

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                startPointX = motionEvent.getX();
                startPointY = motionEvent.getY();

            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                endPointX = motionEvent.getX();
                endPointY = motionEvent.getY();
                distance = endPointX - startPointX;
                if(Math.abs(distance) > 100){
                    if (distance > 0){

                    }else {
                    }
                }else if(distance == 0){
                    if(startPointX - (ScreenWidth/2) > 0){

                    }else {

                    }
                }else{
                }
            }

            return true;
        }
    }
    @Override
    public void onPause(){
//        unregisterReceiver(batteryChangedReceiver);
        ContentValues cv = new ContentValues();
        cv.put("lastposition", currentPage);
        LoadActivity.novalTable.updateValues(cv, "filename = ?", new String[]{fileName});
        super.onPause();

    }





}
