package com.ultrapp.ebookReader.Adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import com.ultrapp.ebookReader.Fragments.Articles;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-10-12
 * Time: 上午11:15
 * To change this template use File | Settings | File Templates.
 */
public class ArticleViewPagerAdapter extends FragmentPagerAdapter {
    private int MaxNum;
    public ArticleViewPagerAdapter(FragmentManager fm, int MaxNum) {
        super(fm);
        this.MaxNum = MaxNum;
    }

    @Override
    public Fragment getItem(int i) {
                return Articles.newInstance(i); //To change body of implemented methods use File | Settings | File Templates.

    }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return MaxNum;
        }
}
