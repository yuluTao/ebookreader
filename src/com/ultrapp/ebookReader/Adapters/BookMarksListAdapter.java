package com.ultrapp.ebookReader.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.ultrapp.ebookReader.Activitys.LoadActivity;
import com.ultrapp.ebookReader.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-29
 * Time: 下午5:26
 * To change this template use File | Settings | File Templates.
 */
public class BookMarksListAdapter extends BaseAdapter{

    private ArrayList<String> bookMarksNames;
    private ArrayList<Integer> markedPages;
    private ArrayList<Boolean> ifSelected;
    private LayoutInflater mLayoutInflater;
    private boolean checkBoxShow;

    public BookMarksListAdapter(Context context, ArrayList<String> bookMarksNames, ArrayList<Integer> markedPages,
                                ArrayList<Boolean> ifSelected, boolean checkBoxShow){
        this.bookMarksNames = bookMarksNames;
        this.markedPages = markedPages;
        this.ifSelected = ifSelected;
        this.checkBoxShow = checkBoxShow;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bookMarksNames.size();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getItem(int i) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int i) {
        return i;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final int p = position;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.bookmark_list_item, null);
        }
        TextView name = (TextView) convertView.findViewById(R.id.book_name);
        TextView rate = (TextView) convertView.findViewById(R.id.rate);
        CheckBox cb = (CheckBox) convertView.findViewById(R.id.checkBox);
        name.setText(bookMarksNames.get(position));
        rate.setText(markedPages.get(position));
        if(checkBoxShow){
            cb.setVisibility(View.VISIBLE);
        }else {
            cb.setVisibility(View.GONE);
        }
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ifSelected.set(p, b);
            }
        });


        return convertView;
    }
}
