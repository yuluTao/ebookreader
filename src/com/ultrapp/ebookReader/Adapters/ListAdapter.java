package com.ultrapp.ebookReader.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ultrapp.ebookReader.Activitys.LoadActivity;
import com.ultrapp.ebookReader.R;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-19
 * Time: 下午8:22
 * To change this template use File | Settings | File Templates.
 */
public class ListAdapter extends BaseAdapter{

    private String[] fileList;
    private LayoutInflater mLayoutInflater;

    public ListAdapter(Context context, String[] flieList){
        this.fileList = flieList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return fileList.length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getItem(int i) {
        return fileList[i];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int i) {
        return i;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.book_list_item, null);
        }
        TextView name = (TextView) convertView.findViewById(R.id.book_name);
        TextView rate = (TextView) convertView.findViewById(R.id.rate);
        Cursor cursor = LoadActivity.novalTable.query("filename=?", new String[] {fileList[position]});
        String bookName = null;
        String readRate = null;
        JSONArray bookIndex = null;
        if(cursor.moveToFirst()){
            bookName = cursor.getString(1);
            if(!cursor.getString(3).equals("")){
                readRate = cursor.getString(2);
                try {
                    bookIndex = new JSONArray(cursor.getString(3));
                    rate.setText(readRate + "/" + (bookIndex.length() -1));
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

        }

        name.setText(bookName);

        return convertView;
    }
}
