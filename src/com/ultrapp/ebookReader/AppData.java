
package com.ultrapp.ebookReader;

import android.app.Application;
import android.content.Context;

/**
 * Created by Issac on 7/18/13.
 */
public class AppData extends Application {
    private static Context sContext;
    private static String databaseName;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        databaseName = getString(R.string.database_name);
    }

    public static Context getContext() {
        return sContext;
    }

    public static String getDatabaseName(){
        return databaseName;
    }

}
