package com.ultrapp.ebookReader.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.ultrapp.ebookReader.AppData;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-10-7
 * Time: 下午4:06
 * To change this template use File | Settings | File Templates.
 */
public class DataBaseSingleton {
    private static DataBaseSingleton mDatabaseSingleton;
    private static SQLiteDatabase db;
    private Context context = AppData.getContext();
    private DataBaseSingleton(){
        db = context.openOrCreateDatabase(AppData.getDatabaseName(), context.MODE_PRIVATE, null);
    }

    public SQLiteDatabase getDatabase(){
        return db;
    }

    public static DataBaseSingleton getDatabaseSingleton(){
        if(mDatabaseSingleton == null){
            mDatabaseSingleton = new DataBaseSingleton();
        }
        return mDatabaseSingleton;
    }
}
