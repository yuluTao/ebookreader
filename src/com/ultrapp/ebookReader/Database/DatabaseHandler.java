package com.ultrapp.ebookReader.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-10-7
 * Time: 下午6:17
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseHandler {
    private SQLiteDatabase db;
    private String tableName;

    public DatabaseHandler( String tableName, SQLiteDatabase db){
        this.db = db;
        this.tableName = tableName;
    }

    public long insert(ContentValues cv){
           return db.insert(tableName, null, cv);
    }

    public int updateValues( ContentValues cv, String where, String[] values){
        return db.update(tableName, cv, where, values);
    }

    public int delete(String where, String[] values){
        return db.delete(tableName, where, values);
    }

    public Cursor query(String where, String[] values){
        return db.query(tableName, null, where, values,null,null,null);
    }

    public Cursor queryColumn(String[] columnName){
        return db.query(tableName,columnName, null,null,null,null,null);
    }

    public Cursor getLatestRows(String limited){
        return db.query(tableName, null, null, null,null,null,"id desc",limited);
    }

}
