package com.ultrapp.ebookReader.Database.Table;

import android.database.sqlite.SQLiteDatabase;
import com.ultrapp.ebookReader.Database.DatabaseHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-10-7
 * Time: 下午6:31
 * To change this template use File | Settings | File Templates.
 */
public class Table extends DatabaseHandler {
    private String tableName;
    private StringBuilder body = new StringBuilder("Create Table if not exists ");
    private SQLiteDatabase db;
    public Table(String tableName, SQLiteDatabase db){
        super(tableName, db);
        this.tableName = tableName;
        this.db = db;
        body.append(tableName + " (");

    }

    public Table addColumn(String columnName, String type, String constraint){
        body.append(" "+ columnName + " " + type + " " + constraint + ",");
        return this;
    }
    public void create(){
        body.deleteCharAt(body.lastIndexOf(",")).append(")");
        db.execSQL(body.toString());
        System.out.println(body.toString());
    }

}
