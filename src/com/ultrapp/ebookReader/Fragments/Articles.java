package com.ultrapp.ebookReader.Fragments;

import android.R;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ultrapp.ebookReader.Activitys.ReadingActivity;
import com.ultrapp.ebookReader.MyView;
import com.ultrapp.ebookReader.Utils.DensityUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-9-20
 * Time: 下午4:39
 * To change this template use File | Settings | File Templates.
 */

public class Articles extends Fragment {
    private int linesCount;
    private String charSet;
    private int ViewWidth;
    private int ViewHeight;
    private int ScreenWidth;
    private Paint mPaint;
    private int PageCount;
    private JSONArray index;
    private MappedByteBuffer m_mbBuf;
    private Canvas mCanvas;
    private int textSize;
    private int sidePadding = 10;
    private int sidePaddingInPx;
    private Bitmap mBitmap;
    private ReadingActivity ra;

    public static Articles newInstance(int num){

        Articles fragment = new Articles();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
//        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        Bundle bundle = getArguments();
        PageCount = bundle.getInt("num") + 1;
        ra = (ReadingActivity) getActivity();
        linesCount = ra.linesCount;
        charSet = ra.charSet;
        ScreenWidth = ra.ScreenWidth;
        ViewHeight = ra.ViewHeight;
        ViewWidth = ra.ViewWidth;
        mPaint = ra.mPaint;
        textSize = ra.textSize;
        m_mbBuf = ra.m_mbBuf;
        index = ra.indexInJSON;
        sidePaddingInPx = DensityUtil.dip2px(getActivity(), sidePadding);
        mBitmap = Bitmap.createBitmap(ScreenWidth, ViewHeight, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        View view = new MyView(getActivity());
        ((MyView)view).setMbitmap(mBitmap);
        try {
            drawPage(PageCount);

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return view;
    }

    @Override
    public void onStop(){

        super.onStop();
        if(mBitmap != null && !mBitmap.isRecycled() ){
            mBitmap.recycle();
        }
    }


    byte[] pageContent;
    long readedBAK;
    long fileLengthBAK;

    public Vector<String> setUpPage(int pageCount) throws IOException, JSONException {
        pageContent = getPageBAK(pageCount);
        readedBAK = 0;
        fileLengthBAK = pageContent.length;
//        long pageStart = 0;
        long pageLength = 0;
        Vector<String> page = new Vector<String>();
//        int readedLinesCount = 0;
//        pageStart = readed ;
        while(page.size() < linesCount ){

            String paragraph = getParagraphBAK();

            pageLength += paragraph.getBytes(charSet).length;
//            if(paragraph.contains("\n")){
//                System.out.println("换行位置：" + paragraph.indexOf("\n"));
//                readedLinesCount ++;
//            }

            // 如果是空白行，直接添加
            if (paragraph.length() == 0) {
                page.add(paragraph);
//                readedLinesCount ++;
            }
            while (paragraph.length() > 0) {
                // 画一行文字
                int nSize = mPaint.breakText(paragraph, true, ViewWidth,
                        null);

                if(paragraph.length() > nSize){
                    String ss = "！，。？；：’”）》】";
                    while(ss.contains(paragraph.substring(nSize,nSize+1))){
                        nSize = nSize - 1 ;
                    }
                }

//                if(pageCount < 5){
//                    System.out.println("计算每行字符：" +nSize);
//                    System.out.println("LinesCount:"+ readedLinesCount + "/" + linesCount
//                            + "\n Content:" + paragraph.substring(0,nSize));
//                }
                page.add(paragraph.substring(0,nSize));
//                readedLinesCount++;

                paragraph = paragraph.substring(nSize);

                if (page.size() >= linesCount) {
                    break;
                }
            }
//            if (paragraph.length() != 0) {
//                pageLength -= paragraph.getBytes(charSet).length;
//
//            }


        }

//            System.out.println("Start:" + pageStart);
//            System.out.println("End:" + (pageStart + pageLength));
//            System.out.println("Count:" + pageCount);
//        long[] nums = new long[]{pageStart, pageLength};
//        index.put(pageCount, nums);
//        readed = pageStart + pageLength;
//
//        pageCount++;


        return page;
    }







    public String getParagraphBAK() throws IOException {
        int len = 0;    //所有读取的内容都使用temp接收
        long start = readedBAK ;

        try{
            while(readedBAK < fileLengthBAK){    //当没有读取完时，继续读取
//            System.out.println("read:" + m_mbBuf.get(readed));
                len++;
                readedBAK++;
                if(pageContent[(int)(readedBAK)] == 10){
//                len++;
//                readed++;
                    break;
                }


            }
        }catch(IndexOutOfBoundsException e){

        }

        byte b[] = new byte[len];
        for(int i = 0; i < len; i++){
            b[i] = pageContent[i + (int)start];
//            System.out.println("byte:" + b[i]);
        }
//        System.out.println("start:"+start);
//        System.out.println("readed:"+readed);
//        System.out.println("len:"+len);
        return new String(b,charSet);
    }

    public byte[] getPageBAK(int pageCount) throws UnsupportedEncodingException, JSONException {
        String pageText;
        long length = ((JSONObject)index.get(pageCount)).getLong("pageLength");
        long position = ((JSONObject)index.get(pageCount)).getLong("pageStart");
        byte[] b = new byte[(int)length];
        for(int i = 0; i < length; i++ ){
            b[i] = m_mbBuf.get((int)(position+i));
        }
        pageText = new String(b,charSet);
        return b;
    }

    public void onDraw(Vector<String> content) {
        for (int i = 0 ; i < content.size(); i ++) {
            mCanvas.drawText(content.get(i), sidePaddingInPx, DensityUtil.dip2px(getActivity(),textSize)*(i+1), mPaint);
        }
    }

    public void drawPage(int pageCount) throws IOException, JSONException {
        Vector<String> content = setUpPage(pageCount);
        ra.bookMarkTitle = content.get(0);
        onDraw(content);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) { //first saving my state, so the bundle wont be empty.
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }


}