package com.ultrapp.ebookReader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * User: Yulu
 * Date: 13-11-26
 * Time: 下午2:56
 * To change this template use File | Settings | File Templates.
 */
public class MyView extends View{
    private Bitmap mbitmap;

    public MyView(Context context) {
        super(context);
    }

    private void drawCurrentPageArea(Canvas canvas, Bitmap bitmap) {

        canvas.save();
        canvas.drawBitmap(bitmap, 0, 0, null);
        canvas.restore();
    }

    public void setMbitmap(Bitmap bitmap){
        mbitmap = bitmap;
    }

    @Override
    public void onDraw(Canvas canvas){
        drawCurrentPageArea(canvas, mbitmap);
    }
}
